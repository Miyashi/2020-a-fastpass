function initialize() {
  var latlng = new google.maps.LatLng(35.68089827634151, 139.56528641103338);
  var opts = {
    zoom: 14,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById("map_canvas"), opts);

  var m_latlng1 = new google.maps.LatLng(35.68447834532266, 139.5638655963333);
  var marker1 = new google.maps.Marker({
    position: m_latlng1,
    map: map
  });

  var m_latlng2 = new google.maps.LatLng(35.684121053805654, 139.5661401094916);
  var marker2 = new google.maps.Marker({
    position: m_latlng2,
    map: map
  });
}