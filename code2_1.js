function initMap() {
  var opts1 = {
    zoom: 15,
    center: new google.maps.LatLng(35.68089827634151, 139.56528641103338)
  };

  var opts2 = {
    zoom: 15,
    center: new google.maps.LatLng(35.68089827634151, 139.56528641103338)
  };

  var opts3 = {
    zoom: 15,
    center: new google.maps.LatLng(35.68089827634151, 139.56528641103338)
  };

  var map1 = new google.maps.Map(document.getElementById("map1"), opts1);

  var map2 = new google.maps.Map(document.getElementById("map2"));
  map2.setOptions(opts2);

  var map3 = new google.maps.Map(document.getElementById("map3"));
  map3.setOptions(opts3);
}